// alert("Hello G");
/* ";" it is a norm for the devs to use semicolons at the end of each statement/line, at this sample, JS does not require the semicolon but since it is the norm, the devs still continue to use semicolon to help code readability.  */

/* "alert()" is one of the builtin codes in JS, these codes have to be written in specific manner so that we can use their function*/

/* "script" tag allows the use of JS functions to be used by the devs when performaing internal scripting*/

/* Comments in JS
	CTRL + / - for one line
	CTRL + SHIFT + / - for multi-line
 	*/

console.log("Hello G")
// console.log allows the browser to display the message that is inside the parenthesis
console.
log
(
 	"Hello Again"
);

// Variables
// it is used to store data
// any information that is used by an application is stored in what we call "memory"
// when we create variables, certain portion of a device's memory is given "name" that we call "variables"

// if the variable has been called without declaring the variable itself, the console would render that the variable that we are calling is "not defined"

// variables and initialize with use of let/const keywords. 
// after declaring the variable - this means that we have created a variable and it is ready to receive data. Failure to assign a value to it would mean that the variable would result to "undefined"

/*
	not defined - does not exist in the codes
	undefined - variable exists but not defined/has no value
*/

// let myVariable;

let myVariable = "hello";

console.log(myVariable);

/*
	Guides in writing variables:
		- using the right keyword is a way for a dev to successfully initialize a variable (let/const)
		- variable names should start with a lowercase character and camelCasing for multiple words
		- variable names should be indicative or descriptive of the value being stored to avoid confusion
*/


// Samples of variable guides

	/*let firstName = "Michael";
	let pokemon = 25000; //a bad naming since we are confused as to what is 25000 gonna do with pokemon

	let FirstName = "Michael"; //not following camelCasing
	let firstName = "Michael";
*/
	// let first name = "Michael"; // it is not advisable to use spaces in between the words of the variables. Common usage of underscores are allowed.


	/*let first_name = "Michael";
	console.log(first_name);
*/

	/*
		Sample of camelCasing are:
			lastName emailAddress mobileNumber
	*/


// Declaring and initializing variables
	// if no keyword, the default variable is "let"

	let productName = "Desktop Computer";
	console.log(productName);

/* Mini Activity 
	productPrice variable with the value of 18999
*/

	let productPrice = 18999;
	console.log(productPrice);

	// In the content of certain application, some variables/information are constant and should not be changed
	// A real world example are loan interest; savings account; or mortgage interest; their values must not be changed due to implications in computation

	const interest = 3.539;
	console.log(interest);


/*
	ReAssigning of variable values
		reassigning a variable value means that we are going to change the initial or previous value into another value

		SYNTAX
			letVariableName = "newValue"
*/

productName = "Laptop";
console.log(productName);

let friend = "GGG";
console.log(friend);

friend = "GGGGGGGGG"
console.log(friend)

friend = "mmmm"
console.log(friend)

friend = "LLLL"
console.log(friend)

// multiple let with the same variable would result in error - "already been declared"

/*interest = 4.489;
console.log(interest)*/

// const variable values cannot and should not be changed by the devs
// if we declare a variable using const, we can neither update nor the variable value cannot be reassigned


/*
	when to use a JS const for a variable?
		as a general rule, always declare a const variable unless you know that the value will change
*/


// ReAssigning v Initializing

let supplier;
//  this is technically an initialization since we are assigning a value to the variable the first time
supplier = "John Smith Tradings";
console.log(supplier)
// this is reassigning since the value has been reassigned

supplier = "Zuitt Store";
console.log(supplier)


// var vs let/const

a = 5;
console.log(a);


// var is also used in declaring a variable, but var is an ECMAScript1 (ES1) feature [ES1 (Javascript 1997)]
// let/const - introduced as new features of ES6 (2015)
/*
	Different of var v let/const
		there are issue when it comes to declared variables using var, regarding hoisting
		in terms of variables, keyword var is hoisted while let/const does not allow hoisting.
		 - hoisting in JS is the default behaviour of moving declartions to the top
*/


// Scope of variables
/*
	scope essentially means where these variables are available for use
	let and const variables are block scoped
	a block is a chunk of codes bounded by {}. a block lives in curly braces, anything within curly brace is a block.
*/

let outerVariable ="hello";
{
	let innerVariable = "hello again";
	console.log(innerVariable);
}

console.log(outerVariable);

// Multiple variable declarations

/*
	Multiple variables can be declared in one line using one keyboard
	the two variable declarations must be separated by a comma
	should the second variable not be changed, use separate declarations;
		let productCode = "CD017";
		const productBrand = "Dell";

	removing keyboards would use let as default, it is advisable that we use the correct keyboard so that devs would have a clue as to what type of variable has been created, or is it for reassigning of values.
*/
let productCode = "CD017", productBrand = "Dell", productStore = "Makati";
console.log(productCode);
console.log(productBrand);
console.log(productStore);


// trying to use a variable with reserved keyword

/*const let = "hello";
console.log(let);
*/

// Uncaught SyntaxError: let is disallowed as a lexically bound name


// Data types in JS
/*
	Strings 
		are series of characters that create a word, a phrase, a sentence or anything related to creating a text
*/

let country = "Philippines";
console.log(country);

// concatenating strings

let province ="Metro Manila";
console.log(province + ", " + country);

// escape character
// \n creates new line break between the next text

let mailAddress = "Muntinlupa\n\nPhilippines";
console.log(mailAddress);

// using double quotes and singles quotes for string data types are valid in JS

// if string has single quote and with apostrophe inside, it is better to use double quotes for string indicator so that we wont have to use the escape char

console.log("John's employees went home early.");
console.log('John\'s employees went home early.');


// numbers /integers / whole numbers

let headcount=26;
console.log(headcount);

// decimal/fractions

let grade=98.7;
console.log(grade);

// exponential notation
let planetDistance = 2e10;
console.log(planetDistance);

// combing strings and numbers
// when numbers and strings are combined, the resulting data type would be a string
console.log("John's grade last quarter is " + grade, + planetDistance);

// true and false for the default value
let isMarried=false;
let inGoodConduct=true;

console.log("isMarried: " + isMarried);
console.log("inGoodConduct " + inGoodConduct);

// Array - are special kind of data type that are used to store multiple values. Arrays can store different data types but it is normally used to store similar data types


/*similar data type
	SYNTAX:
	let/const varName =[elementA, elementB, elementC, ..., elementN];

	counting/position = 0, 1, 2, 3
*/

let grades=[98.7, 92.1, 90.2, 94.6];
console.log(grades);


// different data type

// it is not advisable to use different data tyoes in an array since it would confusing for other devs when they read our codes.

let person=["John", "Smith", 32, true];
console.log(person);

// object data type - objects are another kind of data type thats used to mimic real world objects. they are used to create complete data the contains pieces of information that are relevant to each other

/* SYNTAX
	let/conts varName = {
		propertyA: value, 
		propertyB: value
	}
	*/

let personDetails={
	fullName: "Juan Dela Cruz",
	age:35,
	isMarried: true,
	contact: ["09123456789","09987654321"],
	address:{
		houseNumber:435,
		city:"Manila"
	}
};
console.log(personDetails);

// typeof keyword - used if the dev are not sure or wants to assure of what the data type of the variable is

console.log(typeof personDetails);

const anime=["Naruto", "Slam Dunk", "One Piece"];
console.log(anime);


// anime = ["Akame ga Kill"]; - would return an error because we have const variable
anime[3] = ["Akame ga Kill"];
console.log(anime);

/*
	Constant Arry/Objects
		the const keyword is a little bit misleading when it comes to arrays/objects
		it does not define a constant value for arrays/objects. It defines a constant reference to a value

		we CANNOT
			reassign a constant value
			reassign a constant array
			reassign a constant object

		we CAN
			change the elements of a const array
			change the properties of a const object
*/


// Null datatypes
/*
	null is used to intentionally express the adbsence of a valude inside a variable in declaration/init

	one clear diff of null vs undefined is that null means the variable was created and assigned a value that does not hold any value/amt, compared to the undefined which is concerned with creating a variable but was not given any value
*/

let number=0;
let string=""
console.log(number);
console.log(string);

let jowa=null;
console.log(jowa);

